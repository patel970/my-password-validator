package sheridan;

/**
 * 
 * @author Dhruvil Patel
 *
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	public static boolean hasValidCaseChars (String password) {
		return password.matches(".*[A-Z]+.*");
	}
	
	public static boolean isValidLength( String password ) {
		return (!password.contains(" ") && password.length() >= MIN_LENGTH);
		
	
	}


}
